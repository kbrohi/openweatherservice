package com.openweather.config;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import com.openweather.controller.WeatherController;

@Configuration
@EnableCaching
@EnableScheduling
public class CacheConfig {

	Logger logger = LoggerFactory.getLogger(WeatherController.class);
	@Autowired
	private CacheManager cacheManager;

	@Bean
	@Primary
	public CacheManager cacheManager() {
		SimpleCacheManager cacheManager = new SimpleCacheManager();
		cacheManager.setCaches(Arrays.asList(new ConcurrentMapCache("weatherCityCache"),
				new ConcurrentMapCache("weatherZipCodeCache")));

		return cacheManager;
	}

	@Scheduled(fixedDelay = 2, timeUnit = TimeUnit.MINUTES)
	public void clearCache() {
		cacheManager.getCacheNames().stream().forEach(s -> {
			Cache cache = cacheManager.getCache(s);
			if (cache != null) {
				cache.clear();
				logger.info("{} is cleared", s);
			}
		});
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}