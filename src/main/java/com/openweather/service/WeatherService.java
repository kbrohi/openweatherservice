package com.openweather.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.openweather.config.Configuration;
import com.openweather.model.Response;

@Service
public class WeatherService {

	Logger logger = LoggerFactory.getLogger(WeatherService.class);
	
	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private RestTemplate restTemplate;

	
	@Cacheable(value = "weatherCityCache")
	public ResponseEntity<Object> getWeatherByCity(String city) {
		logger.info("Before getWeatherByCity  city {}",city);
		ResponseEntity<Object> response = findWeatherByCity(city);

        return response;

	}

	public ResponseEntity<Object> findWeatherByCity(String city) {
		logger.info("findWeatherByCity  city {}",city);
		ResponseEntity<Response> cityWeather = restTemplate.
                getForEntity("https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + Configuration.API_KEY, Response.class);

        
        logger.info("Response Lon: " + cityWeather.getBody().getCoord().getLon());
        logger.info("Response Lat: " + cityWeather.getBody().getCoord().getLat());
        
        ResponseEntity<Object> response = restTemplate.
                getForEntity("https://api.openweathermap.org/data/2.5/onecall?lat=" + cityWeather.getBody().getCoord().getLat() +"&lon=" + cityWeather.getBody().getCoord().getLon() + "&appid=" + Configuration.API_KEY, Object.class);
		return response;
	}
	
	@Cacheable(value = "weatherZipCodeCache")
	public ResponseEntity<Object> getWeatherByZipCode(String zipCode, String country) {
		logger.info("Cache Zip Code Object: " + cacheManager.getCache("weatherZipCodeCache"));

        ResponseEntity<Object> response = findWeatherByZipCode(zipCode, country);

        
        
        return response;


	}

	private ResponseEntity<Object> findWeatherByZipCode(String zipCode, String country) {
		logger.info("findWeatherByZipCode  zipCode {}, country {}",zipCode,country);
		ResponseEntity<Object> response = restTemplate.
                getForEntity("https://api.openweathermap.org/data/2.5/weather?zip=" + zipCode + "," + country + "&appid=" + Configuration.API_KEY, Object.class);
		return response;
	}

}
