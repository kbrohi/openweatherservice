package com.openweather.controller;

import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.openweather.service.WeatherService;

@RestController
@RequestMapping("/weather")
public class WeatherController {
	Logger logger = LoggerFactory.getLogger(WeatherController.class);

	@Autowired
	private WeatherService weatherService;

	@Autowired
	private CacheManager cacheManager;  
	
	@RequestMapping(method = RequestMethod.GET, value = "/city/{city}")
    public @ResponseBody Object getWeatherByCity(@PathVariable String city) {
		logger.info("getWeatherByCity  city {}",city);
        ResponseEntity<Object> response = weatherService.getWeatherByCity(city);
        
        return response;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/clear-cache")
    public @ResponseBody Object clearCache() {

		Collection<String> list = cacheManager.getCacheNames();
        
		for(String s: list) {
			
		Cache cache = cacheManager.getCache(s);	
		if(cache !=null) {
			Object ca = cache.getNativeCache();
			logger.info(ca + " : " +s);
		}
		}
		return null;
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/zipcode/{zipCode}/country/{country}")
    public @ResponseBody Object getWeatherByZipCode(@PathVariable String zipCode, @PathVariable String country) {
		ResponseEntity<Object> response = weatherService.getWeatherByZipCode(zipCode, country);
        
        return response;
    }
}
