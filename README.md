1. Used Springboot framework (Java) Rest APIs
2. There are two end points to get weather info 
   => Get Weather info by city name 
        -> URL : i-e: weather/city/Sharjah
   => Get weather info by zip code and country code
        -> URL : i-e weather/zipcode/94040/country/us
3. Spring boot has embedded tomcat server just execute the project and consume APIs
4. Used Open Weather APIs to complete the requirements

Open Weather APIs provide multiple end points to get weather info, by default it shows weather with forcast upto 5 days. As per requirements
we need to display forcast upto 7 days for which latitude and longitude of City (Location) is must, so I consume API to get latitude and longitude
by city name and then get forecast upto 7 days with those biographical values. Stored records to cache for faster response for time period of 10 days,
after 10 days cleared cache with scheduler on back-end to evict all the cache.

In future I'll get weather info by list of cities. The process require to get cities Ids from json by name and call Open Weather API with group ids to get
weather info and map to customize response.

i-e.
 1

I'll need to get the city id for each one you want to call, which can be found here:

http://bulk.openweathermap.org/sample/city.list.us.json.gz http://bulk.openweathermap.org/sample/city.list.json.gz

Then, per the API docs found here https://openweathermap.org/current#severalid we can use the following code to call the data for multiple cities at once:

http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric
